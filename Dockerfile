FROM nginxinc/nginx-unprivileged:1-alpine

LABEL maintainer="amitrathee987@gmail.com"

COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc.nginx.uwsgi_params

ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

# Switch to root user 
USER root

# -p means create dir if dir is already exists then don't create again 
RUN mkdir -p /vol/static/
# 755 means -- ROOT user can read, write and execute
#           -- GROUP and PUBLIC only read and execute not write
RUN chmod 755 /vol/static
# touch create empty file
RUN touch /etc/nginx/conf.d/default.conf
# chown change ownership --> here we change permission nginx to nginx
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

# copy file
COPY ./entrypoint.sh /entrypoint.sh
# chmod +x then file path means to make file executable 
RUN chmod +x /entrypoint.sh

# after finishing, switch back nginx user in place of root user
USER nginx

CMD ["/entrypoint.sh"]